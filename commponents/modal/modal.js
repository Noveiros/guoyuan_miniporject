// commponents/modal/modal.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    show:{
      type: Boolean,
      value: false
    },
    title: {
      type: 'string'
    },
    cancelText: {
      type: 'string',
      value: '取消'
    },
    cancelColor: {
      type: 'string',
      value: "#8B8B8B"
    },
    confirmText: {
      type: 'string',
      value: '确定'
    },
    confirmColor: {
      type: 'string',
      value: '#FFFFFF'
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    key1:''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    click(e){
      var myEventDetail = {
        status: e.currentTarget.dataset.type
      } 
      var myEventOption = {} // 触发事件的选项
      this.triggerEvent('click', myEventDetail, myEventOption)    // 注册一个名字叫做click的一个事件
    },
  },
  options: {
    multipleSlots: true
  }
})