// commponents/goods/goods.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    goodInfo: {
      type: Object,
      value: null,
    },
    type:{
      type:String,
      value:'desc',
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    clickGoods(e) {
      var myEventDetail = this.data.goodInfo;
      var myEventOption = {} // 触发事件的选项
      this.triggerEvent('click', myEventDetail, myEventOption) // 注册一个名字叫做click的一个事件
    }
  }
})