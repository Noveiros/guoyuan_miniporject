const baseURl = 'http://120.24.226.149/api.php';



function request(api, method = "GET", data = {}, loading = true) {
  let methods = method.toLocaleUpperCase();
  if (loading) {
    wx.showLoading({
      title: '加载中.....',
    })
  }
  return new Promise((resolve, reject) => {
    wx.request({
      url: baseURl + '?action=' + api,
      method: methods,
      header: {
        'content-type': methods == 'POST' ? "application/x-www-form-urlencoded" : "application/json" // 默认值
      },
      data: data,
      success: (res) => {
        if (res.data.code == 100) {
          return resolve(res.data.data)
        } else {
          return reject(res)
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
          })
        }
      },
      fail: res => {
        return reject(res)
        wx.showToast({
          title: '网络开小差了，请新尝试',
          icon: 'none',
        })
      },
      complete: res => {
        wx.hideLoading();
      }
    })
  })

}

function post(api, data, loading) {
  return request(api, "POST", data, loading)
}


module.exports = {
  request: request,
  post: post
}