


function router(e) {
  let data = e.currentTarget.dataset;
  let type = data.routertype || "navigateTo" 
  // 3种情况，我们需要去处理一下url参数
  if (type == "navigateTo" || type == "redirectTo" || type == "reLaunch") {
    // 处理url参数
    let newUrl = fromateUrl(data)
    wx[data.routertype]({
      url: newUrl,
      success: res => {

      },
      fail: res => {

      }
    })
  } else if (data.routertype == "navigateBack") {
    let delta = data.delta||1;
    if (delta <= 0) {
      let length = getCurrentPages().length;
      wx.navigateBack({
        delta: length,
        success: res => {

        },
        fail: res => {

        }
      })
    } else {
      wx.navigateBack({
        delta: delta,
        success: res => {

        },
        fail: res => {

        }
      })
    }
  } else if (data.routertype == "switchTab"){
    // 注意，他只能跳到tabber页面去，非tabber页面，会报错
    wx.switchTab({
      url: url,
      success: res=>{
      
      },
      fail: res=>{

      }
    })
  }
}

function fromateUrl(e) {
  let url = e.url + '?'
  for (let key in e) {
    if (typeof e[key] == 'object') {
      if (e[key] !== null) {
        //  url 传参，他只能是key:value 的形式   Object  array  object    =>  转换成字符串
        let value = JSON.stringify(e[key])
        url += key + '=' + value + '&';
      }
    } else {
      url += key + '=' + e[key] + '&';
    }
  }
  console.log(url)
  return url
}




module.exports = {
  toPage: router
}