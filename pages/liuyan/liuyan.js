// pages/liuyan/liuyan.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    form:{
      message:''
    },
    testMessage:'',
    imgUrl:[],
    fileNum:10,
  },



  previewImage(e){
    console.log(e)
    wx.previewImage({
      current: e.currentTarget.dataset.current, // 当前显示图片的http链接
      urls: e.currentTarget.dataset.urls // 需要预览的图片http链接列表
    })
  },
  upload(e){
    // 已经上传的+还未上传的=10  超出的不要
    wx.chooseImage({
      success:res=> {
        const tempFilePaths = res.tempFilePaths
        let yishangchuan = this.data.imgUrl.length;
        let keshangchuan = this.data.fileNum - yishangchuan;
        tempFilePaths.map((item,index)=>{
          if (index < keshangchuan){
            // 模拟一下上传成功后的处理事件
            console.log('上传成功了')
            this.data.imgUrl.push(item);
            this.setData({
              imgUrl:this.data.imgUrl
            })
          }
        })

      }
    })
  },
  del(e){
    console.log(e)
    let slectindex = e.currentTarget.dataset.index;
   let newArray=this.data.imgUrl.filter((item,index)=>{
      return index != slectindex
    })

    this.setData({
      imgUrl: newArray
    })
  },
  input(e){
    console.log(e)
    this.setData({
      [e.currentTarget.dataset.from]: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})