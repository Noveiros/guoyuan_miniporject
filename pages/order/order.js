Page({
  getPageDataNext(){
    if (!this.data.requestStatus) {
      console.log('触底了，但是上页的请求还没结束')
      return
    }
    this.data.page = this.data.page + 1;
    this.getPageData()
  },
  changeCurrent(e) {
    let nextCurrent = e.target.dataset.current;
    if (this.data.current == nextCurrent) {
      return
    }
    this.setData({
      page: 1,
      pageData:[],
      current: e.target.dataset.current
    })
    this.getPageData()
  },
  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    current: 0,
    pageData: [],
    mockData: [{
        order: '1312548550255714',
        url: '/image/index/img@2x.png',
        status: '0',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1312548550255713',
        url: '/image/index/img@2x.png',
        status: '1',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1312548550255724',
        url: '/image/index/img@2x.png',
        status: '2',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1312548550235714',
        url: '/image/index/img@2x.png',
        status: '3',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1312548550655714',
        url: '/image/index/img@2x.png',
        status: '4',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1312548550555714',
        url: '/image/index/img@2x.png',
        status: '6',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1312548570255714',
        url: '/image/index/img@2x.png',
        status: '7',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1312548550755714',
        url: '/image/index/img@2x.png',
        status: '8',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1312535550255714',
        url: '/image/index/img@2x.png',
        status: '9',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      },
      {
        order: '1313548550255714',
        url: '/image/index/img@2x.png',
        status: '10',
        name: '脐橙修剪的4个误区，你必须要纠正！',
        price: '688.00'
      }
    ],
    requestStatus:true,
    timeS:undefined,
    canUseHeight:100
  },

  // fangdou(){
  //   // 这个的运用，淘宝的拍卖那边，2:00以内  有人出价 -> 重新 2：00 倒计时  直至0s没人出价 
  //   if(this.data.timeS){
  //     clearTimeout(this.data.timeS);
  //   }
  //   this.data.timeS=setTimeout(function(){
  //     console.log('执行了')
  //   },5000)
  // },


  getPageData() {
    console.log(`请求分页${this.data.page}数据`)
    if (this.data.requestStatus){
      console.log('发送请求')
      this.data.requestStatus=false
    }else{
      console.log('上次请求还没结束')
      return
    }
    let page = this.data.page
    setTimeout(()=>{
      this.data.pageData[page-1]=this.data.mockData;;
      this.setData({
        pageData:this.data.pageData
      })
      console.log('请求结束')
      this.data.requestStatus=true;
    },1000)
  },
  // 函数防抖
  // 100s   50s  100s
  // 搜索，限制10s一次


  // 函数节流
  // 每点击一次，他就发送一次请求 ，时间还没到，发了请求也没用，那我们就在前端的部分把他拦截掉，不让他发
  // 我前一次请求还没回来，还没有结束，我在发送下一次请求，会出现顺序上的一个错误=》节约服务器的资源
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('order接收到的参数', options)

    this.setData({
      current: options.current||0,  // 保险
    })

    this.getPageData()
    wx.getSystemInfo({
      success: (res)=> {
        console.log(res)
        let bili=750/res.windowWidth;
        console.log(bili)
        let canUseHeight = res.windowHeight - (132 / bili)
        this.setData({
          canUseHeight
        })
      },

    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (!this.data.requestStatus) {
      console.log('触底了，但是上页的请求还没结束')
      return
    }
    this.data.page=this.data.page+1;
    this.getPageData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})