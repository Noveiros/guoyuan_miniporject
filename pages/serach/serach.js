// pages/serach/serach.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    serach_value:'',
    history:[]
  },
  delAll(){
    this.setData({
      history: []
    })
    wx.setStorageSync("search", []) 
  },
  input(e){
    this.setData({
      serach_value: e.detail.value,
    })
  },
  search(e){
    if(this.data.history.length>=10){
      this.data.history.pop()
    }
    this.data.history.unshift(this.data.serach_value)
    wx.setStorageSync("search", this.data.history)  
    this.setData({
      history: this.data.history,
      serach_value:''
    })
  },
  select_item(e){
    console.log(e)
    this.setData({
      serach_value: e.currentTarget.dataset.info
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let history = wx.getStorageSync('search') || [];1
    this.setData({
      history
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})