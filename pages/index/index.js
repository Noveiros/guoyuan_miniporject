// pages/index/index.js
const http = require('../../utils/request.js')
const router = require("../../utils/router.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bannerList: [{
        url: '/image/index/img@2x.png'
      },
      {
        url: '/image/index/img@2x.png'
      },
      {
        url: '/image/index/img@2x.png'
      },
      {
        url: '/image/index/img@2x.png'
      },
      {
        url: '/image/index/img@2x.png'
      },
    ],
    newClass: [{
        url: '/image/index/img@2x.png',
        title: '脐橙修剪的4个误区，你必须要纠正！',
        id: 1
      },
      {
        url: '/image/index/img@2x.png',
        title: '脐橙修剪的4个误区，你必须要纠正！',
        id: 2
      },
      {
        url: '/image/index/img@2x.png',
        title: '脐橙修剪的4个误区，你必须要纠正！',
        id: 3
      },
      {
        url: '/image/index/img@2x.png',
        title: '脐橙修剪的4个误区，你必须要纠正！',
        id: 4
      },
      {
        url: '/image/index/img@2x.png',
        title: '脐橙修剪的4个误区，你必须要纠正！',
        id: 5
      },
      {
        url: '/image/index/img@2x.png',
        title: '脐橙修剪的4个误区，你必须要纠正！',
        id: 6
      },
    ]
  },
  navigateBack(e){
    router.toPage(e);
  },




  click_good(e) {
    console.log(e)
  },
  selectBanner(e) {

    let type = e.target.dataset.item.type;
    if (type == "url") {
      // 外部链接
      console.log('外部链接')
    } else if (type == 'img') {
      // 静态图片
      console.log('静态图片')
    } else if (type == 'detail') {
      // 商品页
      console.log('商品页')
    }
  },
  getPageInfo() {

    wx.request({
      url: 'http://120.24.226.149/api.php?action=getIndex',
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success: (res) => {
        console.log('请求成功后调用', res)
        if (res.data.code == 100) {
          this.setData({
            bannerList: res.data.data.banner
          })
        } else {
          wx.showToast({
            title: res.msg,
          })
        }

      },
      fail: res => {
        // 服务器请求，或者网络问题出现的时候 400  500 
        console.log('仅在失败时候调用', res)
      },
      complete: res => {
        console.log('不管成功或者失败都会调用', res)
      }
    })


  },


  promiseRequset() {
    try{
      http.post('getIndex', {}, true)
        .then((res) => {
          this.setData({
            bannerList: res.banner
          })
          console.log('getIndexInfo')
          return http.post('getListByPage', {
            page: 1
          }, true)
        })
    }catch(err){

    }


    // http.request('getIndex', "POST", {})
    // .then((res)=>{
    //   console.log(res)
    //   this.setData({
    //     bannerList: res.banner
    //   })
    //   console.log('getIndexInfo')
    //   return http.request('getListByPage', 'POST', { page: 1 })
    // })
    // .then(res=>{
    //   console.log('getListByPage1')
    //   console.log(res)
    //   return http.request('getListByPage', 'POST', { page: 2 })
    // })
    // .then(res=>{
    //   console.log('getListByPage2')
    //   console.log(res)
    // })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.promiseRequset()
    // this.getPageInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    console.log('监听到用户下拉操作')
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },


  onShareAppMessage: function(res) {
    if (res.from === 'button') {
      console.log(res.target)
      console.log('转发来自按钮')
    } else {
      console.log('转发来自顶部菜单')
    }
    return {
      title: '我自己定义的一个分享标题',
      path: '/pages/index/index?fromshare=true',
      imageUrl: '/image/index/img@2x.png'
    }
  }
})