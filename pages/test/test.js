// pages/test/test.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '张三', // 姓名
    age: 18, // 年龄
    subject: ['语文', '数学', '英语'], // 学科
    goodsList: [{
        id: 1,
        title: '这是商品名字', // 商品名称
        imgUrl: '/image/index/img@2x.png',
        desc: "简介",
        status: 1,
        price: '60.00', // 价格
        num: 20, // 库存
        order: 2333333333333333
      },
      {
        id: 2,
        title: '这是商品名字', // 商品名称
        imgUrl: '/image/index/img@2x.png',
        desc: "简介",
        status: 1,
        order: 2333333333333333,
        price: '60.00', // 价格
        num: 20 // 库存
      },
      {
        id: 3,
        title: '这是商品名字', // 商品名称
        order: 2333333333333333,
        imgUrl: '/image/index/img@2x.png',
        desc: "简介",
        status: 1,
        price: '60.00', // 价格
        num: 20 // 库存
      },
      {
        id: 4,
        title: '这是商品名字', // 商品名称
        order: 2333333333333333,
        imgUrl: '/image/index/img@2x.png',
        desc: "简介",
        status: 1,
        price: '60.00', // 价格
        num: 20 // 库存
      },
    ],
    isTrue: true, // 布尔值

  },
  select(e) {
    console.log(e.detail)
  },
  toTab() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    console.log('onLoad')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    console.log('onReady')
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    console.log('onShow')
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    console.log('onHide')
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    console.log('onUnload')
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})